package ar.fiuba.tdd.tp1;

public class CellDecimalsFormatter extends CellNumericFormatter {

    public CellDecimalsFormatter() {

        this.formatType = "decimal";

    }

    public CellDecimalsFormatter(CellInterface cell, String decimalsAfterComa) {

        init(cell , decimalsAfterComa);
        this.formatType = "decimal";

    }

    /*
        Si aplicas numericsformatters antes no les da bola, ya sean number como el o
        currency.
     */

    @Override
    public String applyFormat() {

        int decimalsAfterComa = Integer.parseInt(format);

        String stringValue = this.cell.valueAsString();

        if (stringValue.indexOf('.') > -1 && decimalsAfterComa > 0) {
            return stringValue.substring(0, Math.min(stringValue.indexOf('.') + decimalsAfterComa + 1, stringValue.length()));
        }

        String outputString = stringValue.split("\\.")[0];

        for (int i = 0; i < decimalsAfterComa; i++) {

            if (i == 0) {
                outputString = outputString.concat(".");
            }

            outputString = outputString.concat("0");
        }

        return outputString;

    }

    /*
        Redefiniendolo logras que si acumulas numberformatter's vaya cortando
        con la precision de cada uno. Comentandolo podes acumular todos los que quieras
        que solo se va a mostrar el ultimo. El tema es que si lo dejas, es un comportamiento
        usando el valueAsDouble y otro usando el valueAsString ya que en el segundo lo hace
        de una, y no podes hacerlo generico porque cortarias precision viniendo de cell sin
        formato, a menos que las cell sin formato le saques su pre formato de 1 digito
    */

    /*@Override
    public Double valueAsDouble() {

        return Double.parseDouble(valueAsString());

    }*/

    @Override
    public CellFormatter getCreatorElement(TransferPackage transferPackage) {

        return new CellDecimalsFormatter(transferPackage.context,transferPackage.value);

    }

}
