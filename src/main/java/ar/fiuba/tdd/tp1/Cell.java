package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.persisting.CellJsonObject;

import java.util.List;

public class Cell implements CellInterface {

    private Creator<Value> valueCreator = new ValueCreator();
    private Position position;
    private Value value;

    private CellsReferenceManager cellReferenceManager;

    public Cell(Position position, String value, CellsReferenceManager cellReferenceManager) {

        this.position = position;
        this.cellReferenceManager = cellReferenceManager;
        setValueSince(value);

    }

    @Override
    public void setValueSince(String value) {

        TransferPackage packageToBuildValue = new TransferPackage(value,"undefined",this);
        this.value = valueCreator.createFrom(packageToBuildValue);

    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public void setCellReferenceManager(CellsReferenceManager cellReferenceManager) {

        this.cellReferenceManager = cellReferenceManager;

    }

    @Override
    public CellsReferenceManager getCellReferenceManager() {

        return cellReferenceManager;

    }

    @Override
    public Double valueAsDouble() {

        return this.value.valueAsDouble();

    }

    @Override
    public String valueAsString() {

        return this.value.valueAsString();

    }

    @Override
    public void setValueType(String type) {

        TransferPackage packageToBuildValue = new TransferPackage(this.value.getRawValue() , type , this);
        this.value = valueCreator.createFrom(packageToBuildValue);

    }

    @Override
    public String getRawValue() {

        return this.value.getRawValue();

    }

    // agregados por para el acction

    @Override
    public Value getValue() {

        return value;

    }

    @Override
    public void setValue(Value value) {

        this.value = value;

    }

    @Override
    public List<Position> getCellsReferencesPositions() {

        return this.value.getCellsReferencesPositions();

    }

    @Override
    public void setJsonPersistant(CellJsonObject jsonObject) {
        String pos = position.getColumn() + position.getRow();
        jsonObject.setCellBasicValues(this.position.getSheetPosition(), pos, value.getRawValue(), value.getValueType());
    }
}
