package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;

/**
 * Created by fedevm on 18/10/15.
 */
public class Importer {

    Import importLikeThis;
    WorkspacesManager<Workbook> toImport;

    public Importer(Import importLikeThis, WorkspacesManager<Workbook> toImport) {
        this.importLikeThis = importLikeThis;
        this.toImport = toImport;
    }

    public void importFile(String importFile, String bookName, String sheetName) {
        importLikeThis.importFile(importFile, toImport, bookName, sheetName);
    }
}
