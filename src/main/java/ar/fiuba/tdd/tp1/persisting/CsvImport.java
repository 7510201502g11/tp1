package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Sheet;
import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.NoSuchElementException;
import java.util.stream.Stream;

/**
 * Created by fedevm on 18/10/15.
 */
public class CsvImport implements Import {

    // String fileExtension = "csv";
    int actualRow = 1;
    int actualCol = 1;

    @Override
    public void importFile(String fileName, WorkspacesManager<Workbook> workbooks, String bookName, String sheetName) {
        File toImport = new File(fileName);
        Workbook workbook = workbooks.get(bookName);
        Sheet importSheet;
        try {
            importSheet = workbook.getSheet(sheetName);
        } catch (NoSuchElementException ex) {
            workbook.createNewSheet(sheetName);
            importSheet = workbook.getSheet(sheetName);
        }
        if (fileExist(toImport)) {
            processLines(toImport, importSheet);
        }
    }

    private boolean fileExist(File toImport) {
        return toImport.exists() && !toImport.isDirectory();
    }

    private void processLines(File toImport, Sheet importSheet) {
        Stream<String> lines = null;
        try {
            lines = Files.lines(toImport.toPath(), StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        if (lines != null) {
            lines.forEach(line -> processLine(importSheet, line));
        }
    }

    private void processLine(Sheet importSheet, String line) {
        String[] values = line.split(",");
        for (String value : values) {
            if (!value.isEmpty()) {
                importSheet.setCellValue(mapColumnToLetter(actualCol) + String.valueOf(actualRow), value);
            }
            actualCol++;
        }
        actualRow++;
        actualCol = 1;
    }

    private String mapColumnToLetter(int column) {
        int temporal;
        String letter = "";
        while (column > 0) {
            temporal = (column - 1) % 26;
            letter = this.fromCharCode(temporal + 65) + letter;
            column = (column - temporal - 1) / 26;
        }
        return letter;
    }

    private String fromCharCode(int... code) {
        return new String(code, 0, code.length);
    }
}
