package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Workbook;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/**
 * Created by fedevm on 17/10/15.
 */
public class Persistor {

    Persistence persistLikeThis;
    Workbook toPersist;

    public Persistor(Persistence persistence, Workbook workbook) {
        this.persistLikeThis = persistence;
        this.toPersist = workbook;
    }

    public void persistWorkBook(String outName) {
        try {
            this.persistLikeThis.persistWorkbook(this.toPersist, outName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void persistSheet(String sheetName, String outName) {
        this.persistLikeThis.persistSheet(this.toPersist, sheetName, outName);
    }
}
