package ar.fiuba.tdd.tp1.persisting;

import java.util.HashMap;
import java.util.Map;


public class CellJsonObject {
    protected String sheet;
    protected String id;
    protected String value;
    protected String valueType;
    protected Map<String, String> formatter = new HashMap<>();

    public void setCellBasicValues(String sheetName, String id, String value, String valueType) {
        this.sheet = sheetName;
        this.id = id;
        this.value = value;
        this.valueType = valueType;
    }

    public void setCellFormat(String formatType, String format) {
        if (!format.isEmpty()) {
            formatter.put(formatType + ".Format", format);
        }
    }

    public String getSheet() {
        return this.sheet;
    }

    public String getId() {
        return this.id;
    }

    public String getValue() {
        return this.value;
    }

    public Map<String, String> getFormatter() {
        return this.formatter;
    }

    public String getValueType() {

        return valueType;

    }
}
