package ar.fiuba.tdd.tp1;

public interface CreatorElement {

    default <T extends CreatorElement> T createFrom(TransferPackage transferPackage) {

        if ( this.valid(transferPackage) ) {

            transferPackage.created();

            return this.getCreatorElement(transferPackage);

        } else {

            return this.getCreatorInvalidElement();

        }
    }

    default boolean valid(TransferPackage transferPackage) {

        return false;

    }

    default <T extends CreatorElement> T getCreatorElement(TransferPackage transferPackage) {

        return getCreatorInvalidElement();

    }

    <T extends CreatorElement> T getCreatorInvalidElement();

}
