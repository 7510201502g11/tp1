package ar.fiuba.tdd.tp1.tokens;

import java.util.Iterator;
import java.util.LinkedList;

public class MultiExpressionTree implements ExpressionTree {

    private Token token;
    private LinkedList<ExpressionTree> expressionTreeList;
    private ParenthesisSolver parenthesisSolver;

    public MultiExpressionTree(TokensExpression tkExpression, ParenthesisSolver parenthesisSolver) {

        expressionTreeList = new LinkedList<ExpressionTree>();

        this.parenthesisSolver = parenthesisSolver;

        tkExpression = this.parenthesisSolver.mapParenthesis(tkExpression);

        this.token = tkExpression.getMainToken();

        tkExpression = this.parenthesisSolver.getExpressionFor(tkExpression.getRightSideOfTheExpression(this.token).getFirstToken());

        tkExpression = this.parenthesisSolver.mapParenthesis(tkExpression);

        for (TokensExpression i : tkExpression.split()) {
            expressionTreeList.add(i.getMainToken().buildTree(i, this.parenthesisSolver));
        }

    }

    @Override
    public Token getToken() {
        return this.token;
    }

    @Override
    public void showTree() {
        System.out.println(token.toString());
        Iterator<ExpressionTree> iterator = expressionTreeList.iterator();
        while (iterator.hasNext()) {
            iterator.next().showTree();
        }
    }

    @Override
    public ExpressionTree replaceAllKeys() {
        if (this.parenthesisSolver.matches(this.token)) {
            return new MultiExpressionTree(this.parenthesisSolver.getExpressionFor(this.token), this.parenthesisSolver);
        }
        Iterator<ExpressionTree> iterator = expressionTreeList.iterator();
        while (iterator.hasNext()) {
            iterator.next().replaceAllKeys();
        }

        return this;
    }

    @Override
    public String eval() {

        return this.token.evalWithTreeList(this.expressionTreeList);
    }

}
