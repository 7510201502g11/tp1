package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

import java.util.LinkedList;

public class AverageFunctionToken extends TokenFunction {

    @Override
    public String toString() {
        return "AVERAGE";
    }

    @Override
    public Token getToken() {
        return new AverageFunctionToken();
    }

    @Override
    public String result(String[] listOfValuesSplitted) {

        float result = 0;

        for (String s : listOfValuesSplitted) {
            result = result + Float.parseFloat(s);
        }

        return StringFormatter.fmt(result / listOfValuesSplitted.length);
    }

}
