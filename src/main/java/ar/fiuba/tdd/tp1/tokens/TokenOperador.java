package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

import java.util.LinkedList;

public abstract class TokenOperador extends Token {

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();

        syntaxRule.addAllValues();
        syntaxRule.addValidSyntaxToTheRight(new LeftParenthesisToken());
        syntaxRule.addAllFunctions();

        return syntaxRule;
    }

    @Override
    public boolean isOperation() {
        return true;
    }


}
