package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

public class MinFunctionToken extends TokenFunction {

    @Override
    public String toString() {
        return "MIN";
    }

    @Override
    public Token getToken() {
        return new MinFunctionToken();
    }

    @Override
    public String result(String[] listOfValuesSplitted) {

        float result = Float.parseFloat(listOfValuesSplitted[0]);

        for (int i = 1; i < listOfValuesSplitted.length; i++) {
            result = Math.min(Float.parseFloat(listOfValuesSplitted[i]), result);
        }

        return StringFormatter.fmt(result);
    }

}
