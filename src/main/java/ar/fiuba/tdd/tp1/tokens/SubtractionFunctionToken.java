package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.StringFormatter;

public class SubtractionFunctionToken extends TokenFunction {

    @Override
    public Token getToken() {
        return new SubtractionFunctionToken();
    }

    @Override
    public String toString() {
        return "SUBTRACTION";
    }

    @Override
    public String result(String[] listOfValuesSplitted) {

        float result = Float.parseFloat(listOfValuesSplitted[0]);

        for (int i = 1; i < listOfValuesSplitted.length; i++) {
            result = result - Float.parseFloat(listOfValuesSplitted[i]);
        }

        return StringFormatter.fmt(result);
    }

}
