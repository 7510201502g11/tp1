package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.Creator;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class TokenCreator extends Creator<Token> {

    LinkedList<Token> supportedTokens = new LinkedList<>();

    public TokenCreator() {

        addAllSupportedFactoryElements( Arrays
                .asList(new NumericToken() ,
                        new LeftParenthesisToken() ,
                        new CommaToken() ,
                        new RightParenthesisToken() ,
                        new CellToken() ,
                        new RangeToken() ,
                        new TextToken()) );

        addAllOperators();
        addAllFunctions();
    }

    private void addAllOperators() {
        addAllSupportedFactoryElements(Arrays
                .asList(new PlusToken(),
                        new MinusToken(),
                        new MultiplyToken(),
                        new DivisionToken()));
    }

    private void addAllFunctions() {
        addAllSupportedFactoryElements( Arrays
                .asList(new SumToken() ,
                        new SubtractionFunctionToken() ,
                        new ConcatToken(),
                        new MinFunctionToken(),
                        new MaxFunctionToken(),
                        new AverageFunctionToken()));
    }


    private void addAllSupportedFactoryElements(List<Token> creatorElements) {

        getSupportedCreateElements().addAll(creatorElements);

    }

    @Override
    public List<Token> getSupportedCreateElements() {
        return this.supportedTokens;
    }

    @Override
    public Token getCreateInvalidElement() {
        return new InvalidToken();
    }
}
