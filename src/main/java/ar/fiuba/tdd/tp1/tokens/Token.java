package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.CreatorElement;
import ar.fiuba.tdd.tp1.TransferPackage;
import ar.fiuba.tdd.tp1.exceptions.HasNotSyntaxRuleException;

import java.util.LinkedList;

public abstract class Token implements CreatorElement {

    // para poder construir

    @Override
    public boolean valid(TransferPackage transferPackage) {

        return this.toString().equals(transferPackage.value);

    }

    @Override
    public Token getCreatorElement(TransferPackage transferPackage) {

        return getToken();

    }


    @Override
    public Token getCreatorInvalidElement() {

        return new InvalidToken();

    }

    public abstract Token getToken();


    public String toScannerSymbol() {
        return "\\" + toString();
    }

    // para poder evaluar

    public boolean isParenthesis() {
        return false;
    }

    public int getParenthesisCount() {
        return 0;
    }

    public boolean isOperation() {
        return false;
    }

    public boolean isComma() {
        return false;
    }

    public ExpressionTree buildTree(TokensExpression tkExpression, ParenthesisSolver parenthesisSolver) {
        return new BinaryExpressionTree(tkExpression, parenthesisSolver);
    }

    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {
        return "0";
    }

    public int getPriority() {
        return 0;
    }

    public Token getMainToken(Token token) {

        if (this.getPriority() >= token.getPriority()) {

            return this;

        }

        return token;
    }


    // para poder hacer el analisis sintactico

    public SyntaxRule getSyntaxRule() {
        throw new HasNotSyntaxRuleException();
    }

    public String getSyntaxIdentifier() {
        return this.toString();
    }

    public Boolean sameTokenType(Token element) {
        return this.getSyntaxIdentifier().equals(element.getSyntaxIdentifier());
    }

    public String toRawString() {
        return toString();
    }
}
