package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;

import java.util.LinkedList;

public class NumericToken extends Token {

    String numeric;

    public NumericToken() {

    }

    private NumericToken(String numeric) {
        this.numeric = numeric;

    }

    @Override
    public boolean valid(TransferPackage transferPackage) {

        String onlyNumbersRegularExpression = "[-+]?(\\d*[.])?\\d+";
        return transferPackage.value.matches(onlyNumbersRegularExpression);

    }

    @Override
    public Token getCreatorElement(TransferPackage transferPackage) {

        return new NumericToken(transferPackage.value);

    }

    @Override
    public Token getToken() {
        return new NumericToken(this.numeric);
    }

    @Override
    public String toString() {
        return this.numeric;
    }


    @Override
    public String toScannerSymbol() {
        return "null";
    }

    @Override
    public String getSyntaxIdentifier() {
        return "numeric";
    }

    @Override
    public SyntaxRule getSyntaxRule() {

        SyntaxRule syntaxRule = new SyntaxRule();
        syntaxRule.addRulesForNumerics();
        return syntaxRule;
    }

    @Override
    public String evalWithTreeList(LinkedList<ExpressionTree> treeList) {
        return this.numeric;
    }

}
