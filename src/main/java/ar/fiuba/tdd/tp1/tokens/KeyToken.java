package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.exceptions.HasNotScannerSymbol;
import ar.fiuba.tdd.tp1.exceptions.HasNotSyntaxIdentifierException;

public class KeyToken extends Token {

    @Override
    public Token getToken() {
        return new KeyToken();
    }

    @Override
    public String toString() {
        return "$";
    }

    @Override
    public String getSyntaxIdentifier() {

        throw new HasNotSyntaxIdentifierException();
    }

    @Override
    public String toScannerSymbol() {
        throw new HasNotScannerSymbol();
    }

}
