package ar.fiuba.tdd.tp1.tokens;

public class SyntacticAnalizer {

    public Boolean isValidSyntax(TokensExpression tokensExpression) {

        if (tokensExpression.size() >= 2) { // debe tener por lo menos dos tokens

            //validBoundaryCases
            if ( !isValidFirstToken(tokensExpression) || !isValidLastToken(tokensExpression) ) {
                return false;
            }

            for (int i = 1 ; i < tokensExpression.size() - 1 ; i++ ) {

                Token actualToken = tokensExpression.getTokenAt(i);
                SyntaxRule syntaxRule = actualToken.getSyntaxRule();

                if (!syntaxRule.isValidSyntax(actualToken,tokensExpression.getTokenAt(i - 1),tokensExpression.getTokenAt(i + 1))) {
                    //System.out.println(tokensExpression.getTokenAt(i -1 ).toString());
                    //System.out.println(tokensExpression.getTokenAt(i).toString());
                    //System.out.println(tokensExpression.getTokenAt(i +1 ).toString());
                    return false;
                }
            }

        }

        return true;
    }


    private Boolean isValidFirstToken(TokensExpression tokensExpression) {

        SyntaxRule syntaxRule = (tokensExpression.getTokenAt(0)).getSyntaxRule();

        return syntaxRule.isValidSyntaxToTheRight(tokensExpression.getTokenAt(1));
    }

    private Boolean isValidLastToken(TokensExpression tokensExpression) {
        SyntaxRule syntaxRule = (tokensExpression.getTokenAt(tokensExpression.size() - 2)).getSyntaxRule();

        return syntaxRule.isValidSyntaxToTheRight(tokensExpression.getTokenAt(tokensExpression.size() - 1));
    }
}


