package ar.fiuba.tdd.tp1;

import java.util.List;

public class Workbook extends Workspace {

    private WorkspacesManager<Sheet> sheets = new WorkspacesManager(new Sheet());

    public Workbook(String name) {

        this.name = name;
        createNewSheet("default");

    }

    public Workbook() {

    }

    @Override
    public Workbook createNewWorkspace(String name) {
        return new Workbook(name);
    }

    public void createNewSheet(String sheetName) {

        sheets.create(sheetName);
        assignCellReferenceManager(getSheet(sheetName));
    }

    public void removeSheet(String sheetName) {

        sheets.remove(sheetName);
    }

    public void changeNameForSheet(String newName, String sheetName) {

        sheets.changeNameFor(newName, sheetName);

    }

    public Sheet getSheet(String sheetName) {

        return  sheets.get(sheetName);

    }

    public List<String> sheetsNames() {

        return sheets.names();

    }

    private void assignCellReferenceManager(Sheet sheet) {

        sheet.setCellReferenceManager( new CellsReferenceManager(this) );
    }

    public void addSheet(Sheet sheet) {

        sheets.add(sheet);

    }

}
