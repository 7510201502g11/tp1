package ar.fiuba.tdd.tp1.repl;


import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.persisting.CsvPersistence;
import ar.fiuba.tdd.tp1.persisting.Persistor;

public class ExportSheetToCSVCommand extends ConsoleCommand {

    ExportSheetToCSVCommand(ConsoleCommand nextCommand) {
        this.identifier = "exportSheetToCsv";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        String[] nameSplitted = transfer.getInstructionSplitted()[1].split("\\."); // [workbookname, sheetname]

        Workbook workbook = transfer.workBooks.get(nameSplitted[0]);
        Persistor persistor = new Persistor(new CsvPersistence(), workbook);
        persistor.persistSheet( nameSplitted[1], transfer.getInstructionSplitted()[2]);
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.console().writer().println("Worksheet exported to a Csv");
    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'workbookName.worksheetName' 'fileName' ";
    }
}
