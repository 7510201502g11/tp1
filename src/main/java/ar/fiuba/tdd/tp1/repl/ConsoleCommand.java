package ar.fiuba.tdd.tp1.repl;

abstract class ConsoleCommand {

    protected ConsoleCommand next;
    protected String identifier;

    void setNext(ConsoleCommand next) {

        this.next = next;

    }

    ConsoleCommand getNext() {

        return this.next;

    }

    /*
        se aplica si puede, sino sigue la cadena
    */
    void run(CommandTransferPackage transfer) {
        
        if (transfer.getInstructionSplitted()[0].equals(identifier)) {

            try {

                apply(transfer);
                showStatus(transfer);

            } catch (Exception exception) {

                System.console().writer().println("");
                System.console().writer().println("Error Trying to run command");
                System.console().writer().println("");
                System.console().writer().println(exception.getMessage());
                System.console().writer().println("");
            }

        } else {

            next.run(transfer);

        }
    }

    abstract void apply(CommandTransferPackage transfer);

    String getDescription() {

        return this.identifier;

    }

    void showStatus(CommandTransferPackage transfer) {

        System.console().writer().println("");
        showStatusMessage(transfer);
        System.console().writer().println("");

    }

    abstract void showStatusMessage(CommandTransferPackage transfer);


}
