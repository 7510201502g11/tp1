package ar.fiuba.tdd.tp1.repl;

class WorksheetNamesCommand extends NamesCommand {

    WorksheetNamesCommand(ConsoleCommand nextCommand) {

        this.identifier = "worksheetsNames";
        this.next = nextCommand;
    }

    @Override
    void apply(CommandTransferPackage transfer) {

        names = transfer.workBooks.get(transfer.getInstructionSplitted()[1]).sheetsNames();

    }

    @Override
    String getDescription() {

        return this.identifier + " : Example of use; " + this.identifier + " 'workbookName' ";

    }

    @Override
    protected String workspace() {

        return "worksheets";

    }

}