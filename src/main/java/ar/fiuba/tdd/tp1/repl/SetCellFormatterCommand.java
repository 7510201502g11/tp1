package ar.fiuba.tdd.tp1.repl;

import ar.fiuba.tdd.tp1.Sheet;

class SetCellFormatterCommand extends ConsoleCommand {

    @Override
    void apply(CommandTransferPackage transfer) {

        String[] instruction = transfer.getInstructionSplitted();
        String[] id = instruction[1].split("\\.");
        Sheet sheetToOperate = transfer.workBooks.get(id[0]).getSheet(id[1]);

        String formatter = castFormatterIdentifier(instruction[3]);
        String format = castFormatConvention(instruction[4], formatter);

        sheetToOperate.setFormatTo(instruction[2], formatter, format);

    }

    SetCellFormatterCommand(ConsoleCommand next) {

        this.identifier = "setCellFormatter";
        this.next = next;
    }

    @Override
    String getDescription() {
        return this.identifier + " : Example of use; " + this.identifier
                + " 'workbookName.worksheetName cellposition formatter format' ";
    }

    private String castFormatConvention(String format, String formatter) {

        if ( formatter.equals("date") ) {

            format = format.replaceAll("D","d"); // days
            format = format.replaceAll("Y","y"); // years
        }

        return  format;
    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.console().writer().println("FORMAT APPLIED.");
    }

    private String castFormatterIdentifier(String formatter) {

        String castFormatter = formatter;

        if (formatter.equals("format")) {

            castFormatter = "date";

        }

        return castFormatter;
    }

}
