package ar.fiuba.tdd.tp1.repl;


import java.util.List;

class HelpCommand extends ConsoleCommand {

    List<String> commandsDescriptions;

    HelpCommand(List<String> commandsDescriptions, ConsoleCommand next) {

        this.identifier = "help";
        this.commandsDescriptions = commandsDescriptions;
        this.next = next;

    }

    @Override
    void apply(CommandTransferPackage transfer) {

       // do nothing

    }

    @Override
    String getDescription() {

        return this.identifier + " : Return all command list with yours descriptions";

    }

    @Override
    void showStatusMessage(CommandTransferPackage transfer) {

        System.console().writer().println("COMMANDS");
        System.console().writer().println("");
        commandsDescriptions.forEach(
                (String description) ->
                        System.console().writer().println(description)
        );
        System.console().writer().println(getDescription());

    }
}
