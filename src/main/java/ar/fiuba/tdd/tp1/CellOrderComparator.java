package ar.fiuba.tdd.tp1;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by fedevm on 17/10/15.
 */
public class CellOrderComparator implements Comparator<CellInterface>, Serializable {

    @Override
    public int compare(CellInterface o1, CellInterface o2) {
        if (Integer.parseInt(o1.getPosition().getRow()) < Integer.parseInt(o2.getPosition().getRow())) {
            return -1;
        } else if (Integer.parseInt(o1.getPosition().getRow()) > Integer.parseInt(o2.getPosition().getRow())) {
            return 1;
        } else {
            int comparison2 = o1.getPosition().getColumn().compareTo(o2.getPosition().getColumn());
            if (comparison2 != 0) {
                return comparison2;
            }
        }
        return 0;
    }
}
