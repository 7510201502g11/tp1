package ar.fiuba.tdd.tp1;


public abstract class Action {

    protected RecordsManager recordsManager;

    public abstract void execute();

}
