package ar.fiuba.tdd.tp1;

/**
 * Created by gonza on 10/20/15.
 */
public class StringFormatter {

    public static String fmt(float number) {

        if (number == (long) number) {
            return String.format("%d", (long) number);
        } else {
            return String.format("%s", number);
        }
    }
}
