package ar.fiuba.tdd.tp1;


import ar.fiuba.tdd.tp1.acceptance.driver.BadReferenceException;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTestDriver;
import ar.fiuba.tdd.tp1.acceptance.driver.SpreadSheetTester;
import ar.fiuba.tdd.tp1.exceptions.CircularCellsReferencesException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CircularReferenceTests {

    private static final double DELTA = 0.0001;
    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {

        testDriver = new SpreadSheetTester();
    }

    @Test(expected = BadReferenceException.class)
    public void basicCircularReferenceTest() {

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A1");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");

    }

    @Test(expected = BadReferenceException.class)
    public void basicCircularReferenceTestBis() {

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A1");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A2");

    }

    @Test(expected = BadReferenceException.class)
    public void implicitCircularReferenceTest() {

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A1");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");

    }

    @Test(expected = BadReferenceException.class)
    public void selfReferenceTest() {

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A2");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");

    }

    @Test(expected = BadReferenceException.class)
    public void doubleImplicitCircularReferenceTest() {

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A4");
        testDriver.setCellValue("tecnicas", "default", "A4", "= A1");


        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");

    }

    @Test(expected = BadReferenceException.class)
    public void doubleDoubleImplicitCircularReferenceTest() {

        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + A2");
        testDriver.setCellValue("tecnicas", "default", "A2", "= A3");
        testDriver.setCellValue("tecnicas", "default", "A3", "= A4");
        testDriver.setCellValue("tecnicas", "default", "A4", "= A5");
        testDriver.setCellValue("tecnicas", "default", "A5", "= A1");

        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");

    }
}
