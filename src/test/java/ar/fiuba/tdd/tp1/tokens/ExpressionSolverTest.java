package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;
import ar.fiuba.tdd.tp1.tokens.ExpressionSolver;
import ar.fiuba.tdd.tp1.tokens.Scanner;
import ar.fiuba.tdd.tp1.tokens.TokensExpression;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExpressionSolverTest {

    private ExpressionSolver expressionSolver = new ExpressionSolver();

    @Test
    public void solveExpression() {

        String expression = "3*(4+2*4-(3*2))-4";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression , null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 14, 0.01);

    }

    @Test
    public void solveExpressionWithFunction() {

        String expression = "3*(4+2*4-SUM(4,2+4,6))-4";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression , null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), -16, 0.01);

    }

    @Test
    public void solveExpressionWithStrings() {

        String expression = "CONCAT('hola',SUM(4,2),'chau')";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression , null));

        assertEquals(expressionSolver.solveExpression(tokensExpression), "hola6chau");
    }

    @Test
    public void solveWithNegativeNumbers() {

        String expression = "1 + 1 - (-8) + 4";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression , null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 14, 0.01);

    }

    @Test
    public void solveSubtractionFunction() {

        String expression = "SUBTRACTION(4, 3.62)";
        TokensExpression tokensExpression = new Scanner().scan(new TransferPackage(expression , null));

        assertEquals(Float.parseFloat(expressionSolver.solveExpression(tokensExpression)), 0.38, 0.01);
    }
}
