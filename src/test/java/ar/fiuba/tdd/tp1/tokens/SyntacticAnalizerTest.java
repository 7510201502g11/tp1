package ar.fiuba.tdd.tp1.tokens;

import ar.fiuba.tdd.tp1.TransferPackage;
import ar.fiuba.tdd.tp1.tokens.Scanner;
import ar.fiuba.tdd.tp1.tokens.SyntacticAnalizer;
import ar.fiuba.tdd.tp1.tokens.TokensExpression;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/* Estos test, dependen estrechamente del correcto funcionamiento del Scanner */
/*
    OBS: Para validar formulas con referencia de celdas
    hay que crear toda la logica de workbook y sheet
*/
public class SyntacticAnalizerTest {

    private SyntacticAnalizer syntacticAnalizer = new SyntacticAnalizer();
    private Scanner scanner = new Scanner();


    @Test
    public void inValidExpressionWithInvalidSyntaxElements() {


        TokensExpression expression = scanner.scan( new TransferPackage("3+#4" , null));

        assertEquals(false, syntacticAnalizer.isValidSyntax(expression));
    }


    @Test
    public void inValidExpressionWithInvalidSyntaxElementBetweenNumbers() {


        TokensExpression expression = scanner.scan( new TransferPackage("32 ! 3 + 4" , null));
        System.out.println(expression.toString());
        assertEquals(false, syntacticAnalizer.isValidSyntax(expression));
    }


    @Test
    public void inValidExpressionTwoOperatorsTogether() {


        TokensExpression expression = scanner.scan( new TransferPackage("2++4" , null));
        assertEquals(false, syntacticAnalizer.isValidSyntax(expression));
    }

    @Test
    public void inValidExpressionWrongSyntaxForFunctionSum() {

        TokensExpression expression = scanner.scan( new TransferPackage("SUM(+,2+4,6)" , null));
        assertEquals(false, syntacticAnalizer.isValidSyntax(expression));
    }

    @Test
    public void validExpressionWithNumbersOperatorsFunctionsParenthesisAndComma() {

        TokensExpression expression = scanner.scan( new TransferPackage("3*(4+2*4-SUM(4,2+4,6))-4" , null));
        assertEquals(true,syntacticAnalizer.isValidSyntax(expression));
    }


    @Test
    public void validExpressionWithNumbersOperatorsAndParenthesisTest() {
        TokensExpression expression = scanner.scan( new TransferPackage("3*(4+2*4-(3*2))-4" , null));
        assertEquals(true,syntacticAnalizer.isValidSyntax(expression));
    }

    // si no existen las celdas y to-do el mamotrete no van a andar!!!
    /*@Test
    public void validExpressionWithCells() {
        TokensExpression expression = scanner.scan( new TransferPackage("A3 + !caca.A5" , null));
        assertEquals(true,syntacticAnalizer.isValidSyntax(expression));
    }

    @Test
    public void validExpressionWithRangeAndCells() {
        TokensExpression expression = scanner.scan( new TransferPackage("A3 : !caca.A5" , null));
        assertEquals(true,syntacticAnalizer.isValidSyntax(expression));
    }*/

    @Test
    public void validExpressionWithTextAndConcatTokenTest() {
        TokensExpression expression = scanner.scan( new TransferPackage("CONCAT('pepe','amigo')" , null));
        assertEquals(true,syntacticAnalizer.isValidSyntax(expression));
    }

    @Test
    public void validExpressionWithTextAndConcatAndSumTokenTest() {
        TokensExpression expression = scanner.scan( new TransferPackage("CONCAT('pepe',SUM(2,3))" , null));
        assertEquals(true,syntacticAnalizer.isValidSyntax(expression));
    }
}
