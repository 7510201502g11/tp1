package ar.fiuba.tdd.tp1.persisting;

import ar.fiuba.tdd.tp1.Sheet;

import ar.fiuba.tdd.tp1.Workbook;
import ar.fiuba.tdd.tp1.WorkspacesManager;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class JsonPersistorTest {

    private Persistor persistor;
    private Importer importer;
    private WorkspacesManager<Workbook> workbooks;

    @Before
    public void setUp() {
        workbooks = new WorkspacesManager<>(new Workbook());
        workbooks.create("persistorTest");
        workbooks.get("persistorTest").createNewSheet("first");
        Sheet first = workbooks.get("persistorTest").getSheet("first");
        first.setCellValue("S12", "=2+5");
        first.setCellValue("Q5", "persisitira");
        first.setCellValue("V7", "por");
        first.setCellValue("A4", "1992-03-10T00:00:00Z");
        first.setFormatTo("A4", "date", "MM-dd-yyyy");
    }

    @Test
    public void persistAndImportWorkbook() {
        persistor = new Persistor(new JsonPersistence(), workbooks.get("persistorTest"));
        persistor.persistWorkBook("jsonPersist.jss");
        workbooks.get("persistorTest").removeSheet("default");
        workbooks.get("persistorTest").removeSheet("first");
        importer = new Importer(new JsonImport(), workbooks);
        importer.importFile("jsonPersist.jss", "", "");
        Sheet importedSheet = workbooks.get("persistorTest").getSheet("first");
        assertEquals("first", importedSheet.getName());
        assertEquals("persisitira", importedSheet.getCell("Q5").valueAsString());
        assertEquals("7", importedSheet.getCell("S12").valueAsString());
        assertEquals("por", importedSheet.getCell("V7").valueAsString());
        assertEquals("03-10-1992", importedSheet.getCell("A4").valueAsString());

        // File cleanup
        File file1 = new File("jsonPersist.jss");
        file1.delete();
    }
}
